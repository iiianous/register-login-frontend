import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
    html {
        font-family: "Proxima Nova", sans-serif !important;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }

    html, body, #root, #root> div {
        height: 100%
    }

    h1 {}
    h2 { font-size: 1.2rem; }
    h3 {}

    input {
        outline: none;
    }
`;
