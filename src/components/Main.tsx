import React from "react";
import { GlobalStyles } from "../components/GlobalStyles";

function Main<T>(WrappedComponent: React.ComponentType<T>) {
	return (props: T) => (
		<>
			<div className="bg-dark-1 text-white flex items-center justify-center">
				<WrappedComponent {...props} />
			</div>
			<GlobalStyles />
		</>
	);
}

export default Main;
