import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";

import Main from "../Main";
import BaseInput from "../BaseInput";

import { ReactComponent as IconGooggle } from "../../assets/images/search.svg";
import { ReactComponent as IconFacebook } from "../../assets/images/facebook.svg";

type State = {
	email: "";
	password: "";
	error: Error;
};

type Error = {
	password: string;
	email: string;
} | null;

const Login = (): React.ReactElement => {
	// let [form, setForm] = useState<State | null>(null);
	const { register, handleSubmit, errors } = useForm();
	const onSumit = (formData: any) => console.log(formData);
	console.log(errors);
	return (
		<div className="bg-dark-1 text-center">
			<div className="bg-dark-2 p-10 mx-auto rounded-md w-full">
				{/* {JSON.stringify(errors)} */}
				<h3 className="text-lg mb-5">Log in with</h3>

				<div className="flex">
					<button className="flex-1 p-4 bg-dark-1 flex items-center justify-center mr-1 rounded-sm">
						<IconGooggle width="20" />
						<span className="mx-2">Google</span>
					</button>
					<button className="flex-1 p-4 bg-dark-1 flex items-center justify-center ml-1 rounded-sm">
						<IconFacebook width="20" />
						<span className="mx-2">Facebook</span>
					</button>
				</div>
				<h3 className="text-sm my-5">or</h3>
				<form
					autoComplete="off"
					onSubmit={handleSubmit(onSumit)}
					className="text-left"
				>
					<BaseInput
						label="Email"
						type="text"
						name="email"
						rhfRef={register({
							required: true,
						})}
						placeholder="Email address"
						error={errors.email}
					/>
					<BaseInput
						label="Password"
						name="password"
						type="password"
						rhfRef={register({ required: true })}
						placeholder="Password"
						error={errors.password}
					/>
					<button
						type="submit"
						className="bg-blue-400 p-3 w-full rounded-sm mt-5"
					>
						Sign In
					</button>
				</form>
			</div>
			<p className="text-base py-4">
				Don't have account? <Link to="/signup">Sign Up</Link>
			</p>
		</div>
	);
};

export default Main(Login);
