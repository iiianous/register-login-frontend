import React from "react";

type Props = {
	// onChangeHandler: (e: React.ChangeEvent<HTMLInputElement>) => any;
	label: string;
	placeholder: string;
	rhfRef:
		| string
		| ((instance: HTMLInputElement | null) => void)
		| React.RefObject<HTMLInputElement>
		| null
		| undefined;
	// onBlurHandler?: (e: React.ChangeEvent<HTMLInputElement>) => any;
	error?: string | undefined;
	name?: string;
	type?: string;
};

const BaseInput = ({
	label,
	placeholder,
	type,
	name,
	rhfRef,
	error,
}: Props) => {
	return (
		<div className="text-left mt-3">
			<label htmlFor="email" className="text-base mb-1 inline-block">
				{label}
			</label>
			<input
				type={type}
				name={name}
				className="bg-dark-3 w-full p-3 border-color-1 border text-lg"
				placeholder={placeholder}
				ref={rhfRef}
			/>
			{error && <p className="text-red-400 py-2">{label} is required</p>}
		</div>
	);
};

export default BaseInput;
