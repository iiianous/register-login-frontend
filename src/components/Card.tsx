import React from "react";

const Card = (): React.ReactElement => {
	return (
		<div className="rounded-sm p-6 w-1/4">
			<h1>React Card</h1>
		</div>
	);
};

export default Card;
