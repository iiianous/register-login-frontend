import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Login from "../components/pages/Login";
import Signup from "../components/pages/Signup";

const AppRouter = (): React.ReactElement => {
	return (
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={Login} />
				<Route path="/signup" component={Signup} />
			</Switch>
		</BrowserRouter>
	);
};

export default AppRouter;
